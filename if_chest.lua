local function get_formspec(pos)
	local meta = minetest.get_meta(pos)
	local id = meta:get_string("id")
	local inventory = "nodemeta:" .. tostring(pos.x) .. "," .. tostring(pos.y) .. "," .. tostring(pos.z)
	return "size[8,8.5]" ..
		"list[" .. inventory .. ";storage;0,0;8,4]" ..
		"list[current_player;main;0,4.5;8,4]"
end

minetest.register_node("itemflow:if_chest", {
	description = "Chest",
	tiles = {
		"default_node_face.png",
		"default_node_face.png",
		"default_node_face.png^if_chest.png",
		"default_node_face.png^if_chest.png",
		"default_node_face.png^if_chest.png",
		"default_node_face.png^if_chest.png",
	},
	groups = {cracky = 2, itemflow = 2},
	itemflow_type = "storage",
	after_place_node = function(pos, placer, itemstack, pointed_thing)
		local meta = minetest.get_meta(pos)
		meta:set_string("id", "")
		meta:set_string("owner", placer:get_player_name())
		meta:set_string("node_type", "storage")
		local inventory = meta:get_inventory()
		inventory:set_size("storage", 32)
	end,
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		local meta = minetest.get_meta(pos)
		local owner = meta:get_string("owner")
		if owner == player:get_player_name() then
			minetest.show_formspec(player:get_player_name(), "itemflow:if_chest", get_formspec(pos))
		end
	end,
	on_itemflow_configure = "change_infotext",
	can_dig = function(pos, player)
		local meta = minetest.get_meta(pos)
		local inventory = meta:get_inventory()
		if inventory:is_empty("storage") then
			return true
		end
		return false
	end,
})
