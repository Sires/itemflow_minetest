local storage = minetest.get_mod_storage()

function itemflow:add_item(itemstack, to, to_listname, id)
	local to_meta = minetest.get_meta(to)
	local to_inventory = to_meta:get_inventory()
	local from_inventory = itemflow:get_items_in_storage(id)[2]
	local from_container = nil
	local from_index = 0
	for _, container in pairs(from_inventory) do
		for index, item in pairs(container[2]) do
			if item == itemstack then
				from_container = minetest.get_meta(container[1])
				from_container = from_container:get_inventory()
			end
		end
	end
	if to_inventory:room_for_item(to_listname, itemstack) then
		from_container:remove_item("storage", itemstack)
		to_inventory:add_item(to_listname, itemstack)
	else
		return false
	end
end

function itemflow:throw_in_queue(id, priority, pos, listname, stack)
	if not itemflow:contains_item(stack) then
		return false
	end
	local meta = minetest.get_meta(minetest.deserialize(storage:get_string(id)))
	local timer = minetes.get_node_timer(minetest.deserialize(storage:get_string(id)))
	local queue = minetest.deserialize(meta:get_string("queue"))
	table.insert(queue[priority], {pos, listname, stack})
	meta:set_string("queue", minetest.serialize(queue))
	if not timer:is_started() then
		timer:start(1)
	end
end

function itemflow:get_items_in_storage(id)
	local items = {}
	local items_raw = {}
	local meta = minetest.get_meta(minetest.deserialize(storage:get_string(id)))
	local storage = minetest.deserialize(meta:get_string("storage"))
	print("AAAAAAAAAAA" .. meta:get_string("storage"))
	for _, container_pos in pairs(storage) do
		table.insert(items, {container_pos, {}})
		local container_meta = minetest.get_meta(container_pos)
		local container_inventory = container_meta:get_inventory()
		local container_storage = container_inventory:get_list("storage")
		for foo, item in pairs(container_storage) do
			local itemtable = item:to_table()
			table.insert(items[_][2], itemtable)
		end
	end
	return {items, items_raw}
end

function itemflow:convert(list)
	local items = {}
	for _, item in pairs(list) do
		local item_string = nil
		if item:get_wear() > 0 or item:get_metadata() ~= "" then
			local item_string_format_original = item:to_string()
			local item_string_splitted = string.split(item_string_format_original, " ")
			item_string = item_string_splitted[1] .. " 1 " .. item_string_splitted[2] .. " " .. (item_string_splitted[3] or "") .. " " .. (item_string_splitted[4] or "")
		else
			item_string = item:to_string()
		end
		local item_string_splitted = string.split(item_string, " ")
		if item_string_splitted[3] or item_string_splitted[4] then
			table.insert(items, item_string)
		else
			already_exists = false
			for foo, itemstring in pairs(items) do
				if string.split(itemstring, " ")[1] == item_string_splitted[1] then
					local itemstring_splitted = string.split(itemstring, " ")
					local count = tonumber(itemstring_splitted[2]) + tonumber(item_string_splitted[2])
					items[foo] = itemstring_splitted[1] .. " " .. tostring(count)
					already_exists = true
					break
				end
			end
			if not already_exists then
				table.insert(items, item_string)
			end
		end
	end
	return items
end

function itemflow:contains_item(id, itemstring)
	local items = itemflow:get_items_in_storage(id)[2]
	if string.split(itemstring, " ")[3] then
		local special_items = {}
		for _, item in pairs(items) do
			if item:get_wear() > 0 or item:get_meta() ~= "" then
				table.insert(special_items, item)
			end
		end
		items = itemflow:convert(special_items)
		for _, item in pairs(items) do
			if itemstring == item then
				return true
			end
		end
		return false
	else
		items = itemflow:convert(items)
		for _, item in pairs(items) do
			local itemstring_splitted = string.split(itemstring, " ")
			local item_splitted = string.split(item, " ")
			if item_splitted[1] == itemstring_splitted[1] and tonumber(item_splitted[2]) >= tonumber(itemstring_splitted[2]) then
				return true
			else
				return false
			end
		end
	end
end

function itemflow:connect(id, pos, player, node_type)
	local pos = minetest.deserialize(storage:get_string(id))
	local meta = minetest.get_meta(pos)
	local var = minetest.deserialize(meta:get_string(node_type)) or {}
	if string.split(id, ":")[2] then
		if string.split(id, ":")[1] == player then
			table.insert(var, #var + 1, pos)
			meta:set_string(node_type, minetest.serialize(var))
			return #var
		else
			return false
		end
	else
		table.insert(var, #var + 1, pos)
		meta:set_string(node_type, minetest.serialize(var))
		return #var
	end
end

function itemflow:disconnect(id, node_id, node_type)
	local pos = minetest.deserialize(storage:get_string(id))
	local meta = minetest.get_meta(pos)
	local var = minetest.deserialize(meta:get_string(node_type))
	table.remove(var, node_id)
	meta:set_string(node_type, minetest.serialize(var))
end

function itemflow:check_id(id, name)
	if name ~= "" then
		if string.split(id, ":")[2] ~= nil then
			if string.split(id, ":")[1] ~= name then
				print("Pepper " .. name)
				return false
			end
		end
	end
	if storage:get_string(id) then
		return true
	else
		return false
	end
end

local function generate_error(pos, message)
	local meta = minetest.get_meta(pos)
	local errors = minetest.deserialize(meta:get_string("errors"))
	if #errors == 10 then
		table.insert(errors, 10, message)
	else
		table.insert(errors, message)
	end
	meta:set_string("errors", minetest.serialize(errors))
end

local function get_formspec(pos)
	local meta = minetest.get_meta(pos)
	local id = meta:get_string("id")
	local errors = minetest.deserialize(meta:get_string("errors"))
	local textlist = "textlist[0,3;8,4;errorlist;"
	for _, errorr in pairs(errors or {" "}) do
		textlist = textlist .. errorr.. ";"
	end
	textlist = textlist .. "1;false]"
	return "size[8,8]" ..
		"label[0,0;Control Center]" ..
		"field[0.3,1.3;5,1;id;Control center id;" .. id .. "]" ..
		"label[0,2;Use the format <name>:<id> for private control centers]" .. textlist
end

local formspec_pos = {}

minetest.register_node("itemflow:control_center", {
	description = "Control Center",
	tiles = {"default_node_face.png",
		"default_node_face.png",
		"default_node_face.png",
		"default_node_face.png",
		"default_node_face.png",
		"default_node_face.png^control_center.png",},
	groups = {cracky = 2},
	paramtype2 = "facedir",
	after_place_node = function(pos, placer, itemstack, pointed_thing)
		local meta = minetest.get_meta(pos)
		meta:set_string("id", "")
		meta:set_string("infotext", "Not configured control center")
		meta:set_string("queue", minetest.serialize({["minor"] = {}, ["medium"] = {}, ["fast"] = {}}))
		meta:set_string("storage", "{}")
		meta:set_string("crafting", "{}")
		meta:set_string("other", "{}")
		meta:set_string("errors", "{}")
	end,
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		formspec_pos[player:get_player_name()] = pos
		minetest.show_formspec(player:get_player_name(), "itemflow:control_center", get_formspec(pos))
	end,
	on_destruct = function(pos)
		local meta = minetest.get_meta(pos)
		storage:set_string(meta:get_string("id"), "")
	end,
	on_timer = function(pos, elapsed)
		local meta = minetest.get_meta(pos)
		local queue = minetest.deserialize(meta:get_string("queue"))
		local todo = {}
		local priority = ""
		if queue.fast ~= {} then
			todo = queue.fast[1]
			priority = "fast"
		elseif queue.medium ~= {} then
			todo = queue.medium[1]
			priority = "medium"
		elseif queue.minor ~= {} then
			todo = queue.minor[1]
			priority = "minor"
		end
		local itemstring = todo[3]
		if not itemflow:contains_item(meta:get_string("id"), itemstring) then
			generate_error(pos, "Attempt to move items to [" .. minetest.serialize(todo[1]) .. "] failed by not enough items")
		end
		local stack = ItemStack(itemstring)
		local stack_max = stack:get_stack_max()
		local last_items = stack:get_count() <= stack_max
		if not last_items then
			stack:set_count(stack_max)
		end
		local add_item = itemflow:add_item(stack, todo[1], todo[2], meta:get_string("id"))
		if not add_item then
			generate_error(pos, "Attempt to move items to [" .. minetest.serialize(todo[1]) .. "] failed by not enough space")
		end
		if last_items then
			table.remove(queue[priority], 1)
		else
			local old_stack = ItemStack(itemstring)
			local new_stack = old_stack:set_count(old_stack - stack_max)
			local new_itemstring = itemflow:convert({new_stack})
			todo[3] = new_itemstring
			queue[priority][1] = todo
			return true
		end
		return false
	end,
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname ~= "itemflow:control_center" then
		return
	end
	local pos = formspec_pos[player:get_player_name()]
	if not pos then return end
	if fields.quit then formspec_pos[player:get_player_name()] = nil end
	local meta = minetest.get_meta(pos)
	if fields.id ~= meta:get_string("id") then storage:set_string(meta:get_string("id"), "") end
	if fields.id and fields.id ~= "" then
		if storage:get_string(fields.id) == "" then
			if string.split(fields.id, ":")[2] then
				if string.split(fields.id, ":")[1] == player:get_player_name() then
					meta:set_string("id", fields.id)
					storage:set_string(fields.id, minetest.serialize(pos))
					meta:set_string("infotext", "")
				else
					minetest.chat_send_player(player:get_player_name(), "You cannot make a private channel with the name of someone that is not you.")
				end
			else
				meta:set_string("id", fields.id)
				storage:set_string(fields.id, minetest.serialize(pos))
				meta:set_string("infotext", "")
			end
		else
			minetest.chat_send_player(player:get_player_name(), "This channel already exists.")
		end
	end
	return true
end)
