local function get_formspec(pos, have_priority, player)
	local priority = player:get_attribute("configurator_pr")
	if not priority then
		priority = 3
	elseif priority == "Fast" then
		priority = 1
	elseif priority == "Medium" then
		priority = 2
	elseif priority == "Minor" then
		priority = 3
	end
	local id = player:get_attribute("configurator_id")
	if not id then
		id = ""
	end
	local priority_dropdown = "dropdown[0,1;3,1;drd_pr;Fast,Medium,Minor;" .. priority .. "]"
	local formspec = "size[3,3]" ..
		"field[0.3,0.5;3,1;set_id;Set id to:;" .. id .. "]"
	if not have_priority then
		return formspec .. "button[0.75,2;1.5,1;btn_set;Set]"
	else
		return formspec .. priority_dropdown .. "button[0.75,2;1.5,1;btn_set;Set]"
	end
end

local configure_pos = {}

minetest.register_craftitem("itemflow:configurator", {
	description = "Configurator",
	inventory_image = "configurator.png",
	on_place = function(itemstack, placer, pointed_thing)
		if pointed_thing.type == "nothing" or pointed_thing.type == "object" then
			return
		else
			local pos = pointed_thing.under
			local meta = minetest.get_meta(pos)
			local name = minetest.get_node(pos).name
			local itemflow = minetest.registered_nodes[name].groups.itemflow
			if not itemflow then
				return
			else
				if itemflow == 2 then
					minetest.show_formspec(placer:get_player_name(), "itemflow:configure", get_formspec(pos, true, placer))
				else
					minetest.show_formspec(placer:get_player_name(), "itemflow:configure", get_formspec(pos, false, placer))
				end
				configure_pos[placer:get_player_name()] = pos
			end
		end	
	end
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname ~= "itemflow:configure" then
		return
	end
	local pos = configure_pos[player:get_player_name()]
	if not pos then return end
	if fields.quit then configure_pos[player:get_player_name()] = nil end
	local meta = minetest.get_meta(pos)
	local node = minetest.registered_nodes[minetest.get_node(pos).name]
	if fields.set_id ~= player:get_attribute("configurator_id") and fields.set_id then
		if not itemflow:check_id(fields.set_id, player:get_player_name()) then
			minetest.chat_send_player(player:get_player_name(), "Failed to connect to network with id " .. fields.set_id)
		else
			player:set_attribute("configurator_id", fields.set_id)
		end
	end
	if fields.drd_pr then
		player:set_attribute("configurator_pr", fields.drd_pr)
	end
	if fields.btn_set then
		local id = player:get_attribute("configurator_id")
		local pr = player:get_attribute("configurator_pr")
		meta:set_string("id", id)
		if node.groups.itemflow == 2 then
			meta:set_string("priority", pr)
		end
		if node.on_itemflow_configure == "change_infotext" then
			meta:set_string("infotext", "Id: " .. id .. "   Priority: " .. pr)
		elseif node.on_itemflow_configure then
			node.on_itemflow_configure(pos, id, pr)
		end
		itemflow:connect(id, pos, player:get_player_name(), node.itemflow_type)
		print(minetest.serialize(itemflow:convert(itemflow:get_items_in_storage(id))))
	end
end)
